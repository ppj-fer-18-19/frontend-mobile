package com.filip.parkirajme;

import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyAutocompleteListener implements PlaceSelectionListener, View.OnClickListener {
    private Marker currentMarker;
    private PlaceAutocompleteFragment autocompleteFragment;

    public MyAutocompleteListener(PlaceAutocompleteFragment autocompleteFragment) {
        this.autocompleteFragment = autocompleteFragment;
    }

    @Override
    public void onPlaceSelected(Place place) {
        LatLng newMarkerLatLng = place.getLatLng();
        String newMarkerName = place.getName().toString();
        if (currentMarker != null) currentMarker.remove();
        currentMarker = MapsActivity.googleMap.addMarker(new MarkerOptions().position(newMarkerLatLng).title(newMarkerName));
        MapsActivity.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.builder().target(newMarkerLatLng).zoom(15).build()));
    }

    @Override
    public void onError(Status status) {
        // TODO: Handle the error.
        Log.e("placeSelectedError", "An error occurred: " + status);
    }


    @Override
    public void onClick(View v) {
        if (currentMarker != null) {
            autocompleteFragment.setText("");
            currentMarker.remove();
        }
    }
}
