package com.filip.parkirajme;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private LocalBroadcastManager broadcastManager;

    @Override
    public void onCreate() {
        broadcastManager = LocalBroadcastManager.getInstance(this);
    }

    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData());

        Intent in = new Intent();
        in.putExtra("id", remoteMessage.getData().get("id"));
        in.putExtra("occupied", remoteMessage.getData().get("occupied"));
        in.setAction("NOW");
        //sendBroadcast(in);
        broadcastManager.sendBroadcast(in);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i("FCM", "new token: " + s);
    }


}