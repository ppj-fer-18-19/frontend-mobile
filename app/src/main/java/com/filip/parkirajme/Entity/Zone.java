package com.filip.parkirajme.Entity;

import org.json.JSONException;
import org.json.JSONObject;

public class Zone {

    private Integer id;
    private String name;
    private String phone;
    private Double price;
    private String currency;

    public Zone() {
    }

    public Zone(JSONObject jsonObject) {
        try {
            this.id = (Integer) jsonObject.get("id");
            this.name = (String) jsonObject.get("name");
            this.phone = (String) jsonObject.get("phone");
            this.price = (Double) ((JSONObject)jsonObject.get("price")).get("amount");
            this.currency = (String) ((JSONObject)jsonObject.get("price")).get("currency");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return this.getName() + this.getPhone() + this.getCurrency() + this.getPrice();
    }
}
