package com.filip.parkirajme.Entity;

import com.filip.parkirajme.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class ParkingLot implements Device {

    private UUID uuid;
    private LatLng latLng;
    private String parkingName;
    private Zone zone;
    private int occupiedSpaces;
    private int maxSpaces;

    private Marker marker;

    public ParkingLot(JSONObject jsonObject) {
        try {
            this.uuid = UUID.fromString(jsonObject.getString("id"));
            this.latLng = new LatLng(jsonObject.getDouble("latitude"), jsonObject.getDouble("longitude"));
            this.parkingName = jsonObject.getString("name");
            this.occupiedSpaces = jsonObject.getInt("occupiedSpaces");
            this.maxSpaces = jsonObject.getInt("maxSpaces");
            this.zone = new Zone(jsonObject.getJSONObject("zone"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public int getOccupiedSpaces() {
        return occupiedSpaces;
    }

    public ParkingLot setOccupiedSpaces(int occupiedSpaces) {
        this.occupiedSpaces = occupiedSpaces;
        return this;
    }

    public int getMaxSpaces() {
        return maxSpaces;
    }

    public void setMaxSpaces(int maxSpaces) {
        this.maxSpaces = maxSpaces;
    }

    @Override
    public Marker getMarker() {
        return marker;
    }

    @Override
    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    @Override
    public String toString() {
        return this.getUuid() + this.parkingName + this.getLatLng() + this.getOccupiedSpaces() + "/" + this.getMaxSpaces() + this.getZone();
    }

    @Override
    public int getDrawable() {
        return R.drawable.parking;
    }
}
