package com.filip.parkirajme;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessaging;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private PlaceAutocompleteFragment autocompleteFragment;
    public static GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Parkings parkings;
    private InformationWindow informationWindow;

    MyAutocompleteListener myAutocompleteListener;

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        autocompleteFragment.getView().setBackgroundColor(Color.WHITE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("parking-io", "parking-io", importance);
            mChannel.setDescription("parking-io");
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mNotificationManager.createNotificationChannel(mChannel);

            FirebaseMessaging.getInstance().subscribeToTopic("fer")
                    .addOnFailureListener(f -> Log.d("FCM", "failed"))
                    .addOnSuccessListener(f -> Log.d("FCM", "succeed"));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("permission", "svejedno se pozovem svaki put");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setInitialLocation();
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsActivity.googleMap = googleMap;
        googleMap.setPadding(0, 180, 0, 0);
        myAutocompleteListener = new MyAutocompleteListener(autocompleteFragment);
        autocompleteFragment.setOnPlaceSelectedListener(myAutocompleteListener);
        autocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(myAutocompleteListener);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CHECK_SETTINGS);
        }
        else{
            MapsActivity.googleMap.setMyLocationEnabled(true);
            setInitialLocation();
        }

        parkings = new Parkings(this);

        informationWindow = new InformationWindow(findViewById(R.id.cl), parkings.getParkingLotHashMap());
        googleMap.setOnMarkerClickListener(informationWindow);
        googleMap.setOnMapClickListener(informationWindow);
    }

    private void setInitialLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                if(location != null) {
                    LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.builder().target(loc).zoom(15).build()));
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReciever, new IntentFilter("NOW"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReciever);
    }

    private BroadcastReceiver messageReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(parkings != null) {
                parkings.onReceive(context, intent);
            }
        }
    };
}
